//
//  AppCoordinatorProtocol.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-25.
//

import Foundation

protocol AppCoordinatorProtocol: Coordinator {
    func showLoginFlow()
    func showMainFlow()
}
