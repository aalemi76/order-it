//
//  TableViewContainer.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-26.
//

import UIKit
class TableViewContainer: UIView, TableViewProvider {
    var sections: [Sectionable] = []
    var tableView: UITableView
    var dataSourceHandler: TableViewDataSourceHandler
    var delegateHandler: TableViewDelegateHandler
    required init(tableView: UITableView) {
        self.tableView = tableView
        dataSourceHandler = TableViewDataSourceHandler(sections: sections)
        delegateHandler = TableViewDelegateHandler(sections: sections)
        super.init(frame: .zero)
        addSubview(self.tableView)
        self.tableView.pinToEdge(self)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
