//
//  ProductTableViewCell.swift
//  OrderIt
//
//  Created by AliReza on 2022-11-08.
//

import UIKit
import Kingfisher

class ProductTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: ProductTableViewCell.self)
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var discount: UILabel!
    @IBOutlet weak var rating: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        makeCornersRound(for: productImage)
        makeCornersRound(for: discount, with: 20)
    }
    
    func attach(viewModel: Reusable) {
        return
    }
    
    func update(model: Any) {
        
        guard let model = model as? Product else { return }
        
        let url = URL(string: model.thumbnail ?? "")
        productImage.kf.setImage(with: url)
        titleLabel.text = model.title
        descriptionLabel.text = model.productDescription
        discount.text = "%\(Int(model.discountPercentage ?? 0))"
        rating.text = "\(model.rating ?? 0)"
        
    }
    
    private func makeCornersRound(for view: UIView, with cornerRadius: CGFloat = 10) {
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
    }
    
}
