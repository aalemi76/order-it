//
//  PictureTableViewCell.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-26.
//

import UIKit

class PictureTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: PictureTableViewCell.self)
    
    var profilePicView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.layer.cornerRadius = 100
        view.image = UIImage(systemName: "person.crop.circle.fill.badge.plus")
        view.tintColor = GlobalSettings.shared().mainColor
        return view
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        setupImageView()
    }
    
    func setupImageView() {
        profilePicView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(profilePicView)
        NSLayoutConstraint.activate([
            profilePicView.widthAnchor.constraint(equalToConstant: 200),
            profilePicView.heightAnchor.constraint(equalToConstant: 200),
            profilePicView.centerXAnchor.constraint(equalTo: centerXAnchor),
            profilePicView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            profilePicView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)])
    }
    
    func attach(viewModel: Reusable) {
        return
    }
    
    func update(model: Any) {
        if let url = URL(string: UserInfoManager.shared.user?.profilePicURL ?? "") {
            profilePicView.load(url: url)
        }
        return
    }

}
