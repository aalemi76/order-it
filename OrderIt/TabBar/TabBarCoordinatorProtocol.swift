//
//  TabBarCoordinatorProtocol.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-25.
//

import UIKit

protocol TabBarCoordinatorProtocol: Coordinator {
    
    var tabBarController: UITabBarController { get set }
    
}
