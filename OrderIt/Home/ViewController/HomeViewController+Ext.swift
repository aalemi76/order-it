//
//  HomeViewController+Ext.swift
//  OrderIt
//
//  Created by AliReza on 2022-11-08.
//

import UIKit

extension HomeViewController: Viewable {
    func show(result: Result<Any, OrderItError>) {
        
        DispatchQueue.main.async {[weak self] in
            
            switch result {
                
            case .success(let success):
                
                guard let sections = success as? [Sectionable] else {
                    self?.showErrorBanner(title: OrderItError.inconsistentData.rawValue)
                    return
                }
                self?.configureTableView(sections)
                
            case .failure(let failure):
                
                self?.showErrorBanner(title: failure.rawValue)
            }
        }
        
    }
    
    func configureTableView(_ sections: [Sectionable]) {
        tableViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
    }
}
