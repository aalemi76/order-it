//
//  Coordinator.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-22.
//

import UIKit

protocol CoordinatorFinishDelegate: AnyObject {
    func coordinatorDidFinish(childCoordinator: Coordinator)
}

enum CoordinatorType {
    case app, login, tab
}

protocol Coordinator: AnyObject {
    var finishDelegate: CoordinatorFinishDelegate? { get set }
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    var type: CoordinatorType { get }
    init(navigationController: UINavigationController)
    func start()
    func push(viewController: UIViewController, animated: Bool)
    func pop(animated: Bool)
    func present(viewController: UIViewController, animated: Bool)
    func dimiss(viewController: UIViewController, animated: Bool)
    func finish()
}

extension Coordinator {
    func push(viewController: UIViewController, animated: Bool) {
        navigationController.pushViewController(viewController, animated: animated)
    }
    func pop(animated: Bool) {
        navigationController.popViewController(animated: animated)
    }
    func present(viewController: UIViewController, animated: Bool) {
        navigationController.present(viewController, animated: true, completion: nil)
    }
    func dimiss(viewController: UIViewController, animated: Bool) {
        viewController.dismiss(animated: animated, completion: nil)
    }
    func finish() {
        childCoordinators.removeAll()
        finishDelegate?.coordinatorDidFinish(childCoordinator: self)
    }
}
