//
//  Routes.swift
//  OrderIt
//
//  Created by AliReza on 2022-11-08.
//

import Foundation

enum Routes: String {
    
    case baseURL = "https://dummyjson.com"
    case products = "/products"
    case categories = "/products/categories"
    
    static func generateURL(with route: Routes) -> String {
        return Routes.baseURL.rawValue + route.rawValue
    }
    
    static func generateURL(with category: String) -> String {
        return Routes.baseURL.rawValue + "/products" + "/category" + "/\(category)"
    }
}
