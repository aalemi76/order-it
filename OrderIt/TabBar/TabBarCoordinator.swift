//
//  TabBarCoordinator.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-25.
//

import UIKit

enum TabType {
    
    case home
    case cart
    case profile
    
}

class TabBarCoordinator: NSObject, TabBarCoordinatorProtocol, UITabBarControllerDelegate {
    
    var tabBarController: UITabBarController
    
    var finishDelegate: CoordinatorFinishDelegate?
    
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    var type: CoordinatorType { .tab }
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        childCoordinators = []
        tabBarController = UITabBarController()
    }
    
    func start() {
        
        createTab(with: .home)
        createTab(with: .cart)
        createTab(with: .profile)
        
        createTabBar(childCoordinators.map({$0.navigationController}))
        
    }
    
    private func createTab(with tabType: TabType) {
        
        let navigationController = OINavigationController()
        
        switch tabType {
            
        case .home:
            navigationController.tabBarItem = UITabBarItem(title: "Home", image: UIImage(systemName: "house"), selectedImage: UIImage(systemName: "house.fill"))
            let coordinator = HomeCoordinator(navigationController: navigationController)
            coordinator.start()
            childCoordinators.append(coordinator)
            
        case .cart:
            navigationController.tabBarItem = UITabBarItem(title: "Cart", image: UIImage(systemName: "cart"), selectedImage: UIImage(systemName: "cart.fill"))
            let coordinator = CartCoordinator(navigationController: navigationController)
            coordinator.start()
            childCoordinators.append(coordinator)
            
        case .profile:
            navigationController.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(systemName: "person"), selectedImage: UIImage(systemName: "person.fill"))
            let coordinator = ProfileCoordinator(navigationController: navigationController)
            coordinator.finishDelegate = self
            coordinator.start()
            childCoordinators.append(coordinator)
            
        }
    }
    
    private func createTabBar(_ viewControllers: [UIViewController]) {
        tabBarController.delegate = self
        tabBarController.setViewControllers(viewControllers, animated: true)
        tabBarController.selectedIndex = 0
        tabBarController.tabBar.isTranslucent = false
        tabBarController.view.backgroundColor = GlobalSettings.shared().black
        tabBarController.tabBar.backgroundColor = GlobalSettings.shared().mainColor
        tabBarController.tabBar.tintColor = GlobalSettings.shared().black
        tabBarController.tabBar.unselectedItemTintColor = GlobalSettings.shared().darkWhite
        tabBarController.tabBar.layer.masksToBounds = true
        tabBarController.tabBar.layer.cornerRadius = 20
        tabBarController.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        navigationController.viewControllers = [tabBarController]
    }
    
}

extension TabBarCoordinator: CoordinatorFinishDelegate {
    func coordinatorDidFinish(childCoordinator: Coordinator) {
        self.finish()
    }
}
