//
//  User.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-25.
//

import Foundation

struct User: Codable {
    
    var givenName: String?
    var lastName: String?
    var emailAddress: String?
    var profilePicURL: String?
    
    mutating func initUser(from dict: [String: String]) {
        givenName = dict[UserModelKey.givenName.rawValue]
        lastName = dict[UserModelKey.lastName.rawValue]
        emailAddress = dict[UserModelKey.emailAddress.rawValue]
        profilePicURL = dict[UserModelKey.profilePicURL.rawValue]
    }
    
    func getValue(with content: CellType) -> String? {
        switch content {
        case .givenName:
            return givenName
        case .lastName:
            return lastName
        case .email:
            return emailAddress
        }
    }
}

enum UserModelKey: String {
    case givenName
    case lastName
    case emailAddress
    case profilePicURL
    case userToken
}
