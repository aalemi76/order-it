//
//  UserInfoManager.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-25.
//

import Foundation
import GoogleSignIn
import FacebookLogin
import Combine

final class UserInfoManager {
    
    private let retrieveKey = "retrieve user"
    private let appFirstLaunch = "isFirst"
    
    static let shared: UserInfoManager = {
        return UserInfoManager()
    }()
    
    var isSignedIn = false {
        willSet {
            signInStateObserver.send(newValue)
        }
    }
    
    let signInStateObserver = PassthroughSubject<Bool, Never>()
    
    var user: User?
    
    func getUser() throws {
        
        guard let userData = UserDefaults.standard.object(forKey: retrieveKey) as? Data else {
            throw OrderItError.cannotSaveUser
        }
        
        let decoder = JSONDecoder()
        
        let user = try decoder.decode(User.self, from: userData)
        
        isSignedIn = true
        
        self.user = user
        
        return
    }
    
    func setUser(_ user: User) throws {
        
        let encoder = JSONEncoder()
        
        let encoded = try encoder.encode(user)
        
        UserDefaults.standard.set(encoded, forKey: retrieveKey)
        
        UserDefaults.standard.synchronize()
        
        isSignedIn = true
        
        self.user = user
        
        return
    }
    
    func updateUser(_ user: User) throws {
        
        UserDefaults.standard.removeObject(forKey: retrieveKey)
        
        UserDefaults.standard.synchronize()
        
        try setUser(user)
        
        return
        
    }
    
    func signOut() {
        
        LoginManager().logOut()
        GIDSignIn.sharedInstance.signOut()
        
        UserDefaults.standard.removeObject(forKey: retrieveKey)
        
        UserDefaults.standard.synchronize()
        
        isSignedIn = false
        
        user = nil
        
        return
    }
    
    func isFirstLaunch() -> Bool {
        if let isFirstLaunch = UserDefaults.standard.object(forKey: appFirstLaunch) as? Bool {
            return isFirstLaunch
        } else {
            UserDefaults.standard.set(false, forKey: appFirstLaunch)
            return true
        }
    }
    
}
