//
//  ProfileViewController+Ext.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-26.
//

import Foundation

extension ProfileViewController: Viewable {
    
    func show(result: Result<Any, OrderItError>) {
        
        switch result {
            
        case .success(let sections):
            
            guard let sections = sections as? [Sectionable] else {
                showErrorBanner(title: OrderItError.inconsistentData.rawValue)
                return
            }
            
            configureTableView(sections)
            
        case .failure(let error):
            
            showErrorBanner(title: error.rawValue)
        }
    }
    
    func configureTableView(_ sections: [Sectionable]) {
        tableViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
    }
}
