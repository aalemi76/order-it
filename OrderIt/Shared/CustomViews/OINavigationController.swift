//
//  OINavigationController.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-23.
//

import UIKit

class OINavigationController: UINavigationController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = GlobalSettings.shared().mainColor
        appearance.titleTextAttributes = [.foregroundColor: GlobalSettings.shared().black ?? .white]
        appearance.largeTitleTextAttributes = [.foregroundColor: GlobalSettings.shared().black ?? .white]
        navigationBar.tintColor = .white
        navigationBar.standardAppearance = appearance
        navigationBar.compactAppearance = appearance
        navigationBar.scrollEdgeAppearance = appearance
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func configureView() {
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: GlobalSettings.shared().systemFont(type: .bold, size: 18)]
        navigationBar.titleTextAttributes = textAttributes
        navigationBar.barTintColor = GlobalSettings.shared().mainColor
        navigationBar.tintColor = .white
        navigationBar.shadowImage = UIImage()
    }

}
