//
//  CartCoordinator.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-26.
//

import UIKit

class CartCoordinator: Coordinator {
    var finishDelegate: CoordinatorFinishDelegate?
    
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    var type: CoordinatorType
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.childCoordinators = []
        self.type = .tab
    }
    
    func start() {
        let viewController = CartViewController()
        viewController.title = "Shopping Cart"
        push(viewController: viewController, animated: true)
    }
    
    
}
