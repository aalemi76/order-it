//
//  LoginViewController+Ext.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-25.
//

import Foundation

extension LoginViewController: Viewable {
    
    func show(result: Result<Any, OrderItError>) {
        
        switch result {
        case .success:
            didFinishSignInFlow.send(true)
            
        case .failure(let error):
            showErrorBanner(title: error.rawValue)
            
        }
        return
    }
    
}
