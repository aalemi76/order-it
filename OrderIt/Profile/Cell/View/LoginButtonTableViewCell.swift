//
//  LoginButtonTableViewCell.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-28.
//

import UIKit
import Combine

class LoginButtonTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: LoginButtonTableViewCell.self)
    
    var viewModel: Reusable?
    
    var cancellableStore = Set<AnyCancellable>()
    
    var loginButton: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = GlobalSettings.shared().mainColor
        btn.setTitleColor(GlobalSettings.shared().black, for: .normal)
        btn.titleLabel?.font = GlobalSettings.shared().systemFont(type: .bold, size: 18)
        return btn
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(loginButton)
        NSLayoutConstraint.activate([
            loginButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            loginButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            loginButton.heightAnchor.constraint(equalToConstant: 40),
            loginButton.widthAnchor.constraint(equalToConstant: 100),
            loginButton.topAnchor.constraint(equalTo: topAnchor, constant: 10)])
        loginButton.layer.cornerRadius = 15
        UserInfoManager.shared.signInStateObserver.sink { [weak self] _ in
            self?.update(model: "")
        }.store(in: &cancellableStore)
        loginButton.addTarget(self, action: #selector(didSelectLoginButton), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func attach(viewModel: Reusable) {
        self.viewModel = viewModel
    }
    
    func update(model: Any) {
        if UserInfoManager.shared.isSignedIn {
            loginButton.setTitle("Logout", for: .normal)
        } else {
            loginButton.setTitle("Login", for: .normal)
        }
    }
    
    @objc func didSelectLoginButton() {
        (viewModel as? LoginCellViewModel)?.didTapLoginButton()
    }
    
}
