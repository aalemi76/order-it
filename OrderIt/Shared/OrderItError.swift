//
//  OrderItError.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-25.
//

import Foundation

enum OrderItError: String, Error {
    case cannotSaveUser = "Unable to save user information."
    case cannotRetrieveUser = "Unable to retrieve user information."
    case inconsistentData = "The data is inconsistent with expected type."
    case couldNotFindProduct = "Sorry, we couldn't find the product."
}
