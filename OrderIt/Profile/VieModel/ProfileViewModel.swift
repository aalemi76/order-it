//
//  ProfileViewModel.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-26.
//

import Foundation
import Combine

class ProfileViewModel: ViewModelProvider {
    
    weak var view: Viewable?
    
    let didTapLoginButton = PassthroughSubject<Bool, Never>()
    
    var cancellableStore = Set<AnyCancellable>()
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        createCells()
        observeOnUserState()
    }
    
    func observeOnUserState() {
        UserInfoManager.shared.signInStateObserver.sink { [weak self] _ in
            self?.createCells()
        }.store(in: &cancellableStore)
    }
    
    func observeOnLoginButtonTouch(viewModel: LoginCellViewModel) {
        
        viewModel.didSelectLoginButton.sink { [weak self] didTap in
            self?.didTapLoginButton.send(didTap)
            UserInfoManager.shared.signOut()
        }.store(in: &cancellableStore)
        
    }
    
    func createCells() {
        
        var sections = [Sectionable]()
        
        if UserInfoManager.shared.isSignedIn {
            
            let profileSection = SectionProvider(title: nil, cells: [TableCellViewModel(reuseID: PictureTableViewCell.reuseID, cellClass: PictureTableViewCell.self, model: "")], headerView: nil, footerView: nil)
            
            let givenNameSection = SectionProvider(title: "First Name", cells: [TableCellViewModel(reuseID: TextTableViewCell.reuseID, cellClass: TextTableViewCell.self, model: CellType.givenName)], headerView: nil, footerView: nil)
            
            let lastNameSection = SectionProvider(title: "Last Name", cells: [TableCellViewModel(reuseID: TextTableViewCell.reuseID, cellClass: TextTableViewCell.self, model: CellType.lastName)], headerView: nil, footerView: nil)
            
            let emailSection = SectionProvider(title: "Email address", cells: [TableCellViewModel(reuseID: TextTableViewCell.reuseID, cellClass: TextTableViewCell.self, model: CellType.email)], headerView: nil, footerView: nil)
            
            let loginCellViewModel = LoginCellViewModel(reuseID: LoginButtonTableViewCell.reuseID, cellClass: LoginButtonTableViewCell.self, model: "")
            
            observeOnLoginButtonTouch(viewModel: loginCellViewModel)
            
            let loginSection = SectionProvider(title: nil, cells: [loginCellViewModel], headerView: nil, footerView: nil)

            sections = [profileSection, givenNameSection, lastNameSection, emailSection, loginSection]
            
        } else {
            
            let loginCellViewModel = LoginCellViewModel(reuseID: LoginButtonTableViewCell.reuseID, cellClass: LoginButtonTableViewCell.self, model: "")
            
            observeOnLoginButtonTouch(viewModel: loginCellViewModel)
            
            let loginSection = SectionProvider(title: nil, cells: [loginCellViewModel], headerView: nil, footerView: nil)
            
            
            sections = [loginSection]
            
        }
        
        view?.show(result: .success(sections))
    }
    
    
}
