//
//  HomeViewController.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-25.
//

import UIKit

class HomeViewController: SharedViewController {

    let viewModel: ViewModelProvider
    
    private lazy var tableView: UITableView = {
        let tbl = UITableView(frame: .zero, style: .insetGrouped)
        tbl.backgroundColor = GlobalSettings.shared().black
        tbl.tableFooterView = nil
        tbl.sectionHeaderHeight = 0
        tbl.sectionFooterHeight = 0
        tbl.separatorStyle = .none
        tbl.rowHeight = UITableView.automaticDimension
        tbl.estimatedRowHeight = 144
        return tbl
    }()
    
    lazy var tableViewContainer: TableViewContainer = {
        let container = TableViewContainer(tableView: tableView)
        container.backgroundColor = GlobalSettings.shared().black
        return container
    }()
    
    private(set) var catgories = [String]()
    var selectedCategory = "All" {
        didSet{
            (viewModel as? HomeTabViewModel)?.categoryDidChange(newCategory: selectedCategory)
        }
    }
    
    private lazy var dropDownButton: UIButton = {
        let btn = UIButton()
        btn.contentMode = .scaleAspectFill
        btn.setBackgroundImage(UIImage(systemName: "list.bullet.circle.fill"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.tintColor = GlobalSettings.shared().mainColor
        return btn
        
    }()
    
    init(viewModel: ViewModelProvider) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .none
        viewModel.viewDidLoad(self)
        addTableViewContainer()
        print(tabBarController?.tabBar)
    }
    
    func addTableViewContainer() {
        tableViewContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableViewContainer)
        NSLayoutConstraint.activate([
            tableViewContainer.topAnchor.constraint(equalTo: view.topAnchor),
            tableViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableViewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
        addFilterButton()
    }
    
    func addFilterButton() {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 60))
        view.addSubview(dropDownButton)
        NSLayoutConstraint.activate([
            dropDownButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
            dropDownButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            dropDownButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10),
            dropDownButton.widthAnchor.constraint(equalToConstant: 40)
        ])
        dropDownButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
        tableView.addSubview(view)
        tableView.tableHeaderView = view
    }
    
    func setCategories(_ categories: [String]) {
        
        self.catgories = categories
        
    }
    
    @objc func showMenu() {
        
        let alertVC = UIAlertController(title: "Category", message: "Please select a category", preferredStyle: .actionSheet)
        
        catgories.forEach { category in
            if category == selectedCategory {
                alertVC.addAction(UIAlertAction(title: category, style: .destructive){ [weak self] action in
                    self?.selectedCategory = action.title ?? "All"
                })
            } else {
                alertVC.addAction(UIAlertAction(title: category, style: .default){ [weak self] action in
                    self?.selectedCategory = action.title ?? "All"
                })
            }
        }
        
        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        
        present(alertVC, animated: true)
        
    }

}
