//
//  TextTableViewCell.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-26.
//

import UIKit

enum CellType {
    
    case givenName
    case lastName
    case email
    
}

class TextTableViewCell: UITableViewCell, Updatable {

    static let reuseID = String(describing: TextTableViewCell.self)
    
    lazy var label: UILabel = {
        let label = UILabel()
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        NSLayoutConstraint.activate([
            label.centerYAnchor.constraint(equalTo: centerYAnchor),
            label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            label.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            label.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)])
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
    }
    
    func attach(viewModel: Reusable) {
        return
    }
    
    func update(model: Any) {
        guard let cellType = model as? CellType else { return }
        
        setTitle(with: cellType)
    }
    
    func setTitle(with content: CellType) {
        label.textColor = GlobalSettings.shared().mainColor
        label.font = GlobalSettings.shared().systemFont(type: .semiBold, size: 18)
        label.text = UserInfoManager.shared.user?.getValue(with: content)
    }

}
