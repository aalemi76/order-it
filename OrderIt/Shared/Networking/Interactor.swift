//
//  Interactor.swift
//  OrderIt
//
//  Created by AliReza on 2022-11-08.
//

import Foundation
import Alamofire

class Interactor<Model: Codable> {
    
    typealias Object = Model
    
    private func makeParameters(skip: Int?, limit: Int?) -> [String: String] {
        
        var params = [String: String]()
        var skipString = ""
        if let skip = skip {
            skipString = "\(skip)"
        }
        var limitString = ""
        if let limit = limit {
            limitString = "\(limit)"
        }
        params = ["skip": skipString, "limit": limitString]
        
        return params
    }
    
    func getModel(_ url: String, method: HTTPMethod = .get, encoding: URLEncoding = .queryString, skip: Int? = nil, limit: Int? = nil, headers: [String: String]? = nil, onSuccess: @escaping (Object) -> Void, onFailure: @escaping (OrderItError) -> Void) {
        
        let params = makeParameters(skip: skip, limit: limit)
        
        AF.request(url, method: method, parameters: params, encoding: encoding, interceptor: nil, requestModifier: nil).responseDecodable(of: Model.self, queue: .global(qos: .background)) { response in
            if response.response?.statusCode == 404 {
                onFailure(.couldNotFindProduct)
                return
            }
            guard let object = response.value else {
                onFailure(.inconsistentData)
                return
            }
            
            onSuccess(object)
            
        }
        
    }
}
