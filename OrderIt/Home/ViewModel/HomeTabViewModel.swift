//
//  HomeTabViewModel.swift
//  OrderIt
//
//  Created by AliReza on 2022-11-08.
//

import Foundation

class HomeTabViewModel: ViewModelProviderMappable {
    
    typealias model = HomeTabModel
    
    private let interactor: Interactor<HomeTabModel>
    
    private weak var view: Viewable?
    
    private var sections = [Sectionable]()
    
    required init(interactor: Interactor<HomeTabModel>) {
        self.interactor = interactor
    }
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        let url = Routes.generateURL(with: .products)
        fetchData(url)
        getCategories()
    }
    
    func categoryDidChange(newCategory: String) {
        let category = newCategory.lowercased().replacingOccurrences(of: " ", with: "-")
        if category == "all" {
            let url = Routes.generateURL(with: .products)
            fetchData(url)
        } else {
            let url = Routes.generateURL(with: category)
            fetchData(url)
        }
    }
    
    private func fetchData(_ url: String) {
        interactor.getModel(url) { [weak self] homeTabModel in
            guard let products = homeTabModel.products else { return }
            self?.setSections(with: products)
            self?.view?.show(result: .success(self?.sections ?? ""))
        } onFailure: { [weak self] error in
            self?.view?.show(result: .failure(error))
        }

    }
    
    private func setSections(with product: [Product]) {
        let cells = product.map({ TableCellViewModel(reuseID: ProductTableViewCell.reuseID, cellClass: ProductTableViewCell.self, model: $0) })
        sections = [SectionProvider(title: nil, cells: cells, headerView: nil, footerView: nil)]
//        guard let section = sections.first else {
//            sections = [SectionProvider(title: nil, cells: cells, headerView: nil, footerView: nil)]
//            return
//        }
//        section.append(cells)
    }
    
    private func getCategories() {
        let interactor = Interactor<[String]>()
        let url = Routes.generateURL(with: .categories)
        interactor.getModel(url) { [weak self] categories in
            var temp = categories.map({ cat in
                var new = cat.capitalized
                new = new.replacingOccurrences(of: "-", with: " ")
                return new
            })
            temp.insert("All", at: 0)
            (self?.view as? HomeViewController)?.setCategories(temp)
        } onFailure: { error in
            print(error)
        }

    }
    
    
}
