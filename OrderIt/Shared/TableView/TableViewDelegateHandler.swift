//
//  TableViewDelegateHandler.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-26.
//

import UIKit
import Combine

class TableViewDelegateHandler: NSObject, UITableViewDelegate {
    private var sections: [Sectionable]
    var passSelectedItem = PassthroughSubject<Any, Never>()
    var passSelectedSection = PassthroughSubject<Int, Never>()
    var scrollViewDidScroll = PassthroughSubject<UIScrollView, Never>()
    init(sections: [Sectionable]) {
        self.sections = sections
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = sections[section].getHeaderView()
        return header
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = sections[section].getFooterView()
        return footer
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as? UITableViewHeaderFooterView)?.textLabel?.textColor = GlobalSettings.shared().darkWhite
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = sections[indexPath.section].getCells()[indexPath.row].getModel()
        passSelectedItem.send(item)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollViewDidScroll.send(scrollView)
    }
}
