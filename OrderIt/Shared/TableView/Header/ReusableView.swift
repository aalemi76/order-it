//
//  ReusableView.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-26.
//

import Foundation
protocol ReusableView {
    func getReuseID() -> String
    func getViewClass() -> AnyClass
}
