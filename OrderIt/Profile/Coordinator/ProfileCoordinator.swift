//
//  ProfileCoordinator.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-26.
//

import UIKit
import Combine

class ProfileCoordinator: Coordinator {
    
    var finishDelegate: CoordinatorFinishDelegate?
    
    var cancellableStore = Set<AnyCancellable>()
    
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    var type: CoordinatorType
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.type = .tab
        self.childCoordinators = []
    }
    
    func start() {
        let viewModel = ProfileViewModel()
        let viewController = ProfileViewController(viewModel: viewModel)
        viewController.title = "Profile Details"
        push(viewController: viewController, animated: true)
        viewModel.didTapLoginButton.sink { [weak self] _ in
            self?.finish()
        }.store(in: &cancellableStore)
    }
    
    
}
