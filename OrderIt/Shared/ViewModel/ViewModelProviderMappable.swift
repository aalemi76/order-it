//
//  ViewModelProviderMappable.swift
//  OrderIt
//
//  Created by AliReza on 2022-11-08.
//

import Foundation

protocol ViewModelProviderMappable: ViewModelProvider {
    associatedtype model: Codable
    init(interactor: Interactor<model>)
}
