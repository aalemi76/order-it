//
//  LoginViewController.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-23.
//

import UIKit
import GoogleSignIn
import FacebookLogin
import Combine

class LoginViewController: SharedViewController {
    
    var buttonWidth: CGFloat = 256
    var buttonHeight: CGFloat = 40
    
    lazy var loginProviderStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .center
        view.distribution = .fill
        view.isUserInteractionEnabled = true
        view.autoresizesSubviews = true
        view.spacing = 20
        return view
    }()
    
    lazy var imageView = UIView()
    
    var userInfo: [String: String] = [:]
    
    var didFinishSignInFlow = PassthroughSubject<Bool, OrderItError>()
    
    let viewModel: ViewModelProvider
    
    init(viewModel: ViewModelProvider) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func didSelectGoogleSignIn() {
        (viewModel as? LoginViewModel)?.didTapGoogleSignIn()
    }
    
    @objc func didSelectFBLogin() {
        (viewModel as? LoginViewModel)?.didTapFacebookLogin()
    }
    
    @objc func didSelectSkipButton() {
        didFinishSignInFlow.send(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad(self)
        setUpViewComponents()
    }
    
    func setUpViewComponents() {
        view.backgroundColor = GlobalSettings.shared().black
        
        loginProviderStackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(loginProviderStackView)
        NSLayoutConstraint.activate([
            loginProviderStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loginProviderStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            loginProviderStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            loginProviderStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50)])
        
        addLoginImage()
        addFBLoginButton()
        addGoogleSignInButton()
        addSkipButton()
        
        UIView.animate(withDuration: 1, delay: 0.5, usingSpringWithDamping: 40, initialSpringVelocity: 25) { [weak self] in
            self?.imageView.alpha = 100
            self?.imageView.frame.origin.y += UIScreen.main.bounds.height/3
        }
    }
    
    func addGoogleSignInButton() {
        let button = UIButton()
        button.setImage(UIImage(named: "Google Logo"), for: .normal)
        button.setTitle("  Continue with Google", for: .normal)
        button.backgroundColor = .white
        button.titleLabel?.font = GlobalSettings.shared().systemFont(type: .semiBold, size: 15)
        button.setTitleColor(.darkGray, for: .normal)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(didSelectGoogleSignIn), for: .touchUpInside)
        NSLayoutConstraint.activate([
            button.widthAnchor.constraint(equalToConstant: buttonWidth),
            button.heightAnchor.constraint(equalToConstant: buttonHeight)
        ])
        loginProviderStackView.addArrangedSubview(button)
        
    }
    
    func addFBLoginButton() {
        let button = UIButton()
        button.setImage(UIImage(named: "Facebook Logo"), for: .normal)
        button.setTitle("  Continue with Facebook", for: .normal)
        button.backgroundColor = GlobalSettings.shared().fbBlue
        button.titleLabel?.font = GlobalSettings.shared().systemFont(type: .semiBold, size: 15)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(didSelectFBLogin), for: .touchUpInside)
        NSLayoutConstraint.activate([
            button.widthAnchor.constraint(equalToConstant: buttonWidth),
            button.heightAnchor.constraint(equalToConstant: buttonHeight)
        ])
        loginProviderStackView.addArrangedSubview(button)
    }
    
    func addSkipButton() {
        let button = UIButton()
        button.setImage(UIImage(named: "Small Logo"), for: .normal)
        button.setTitle("  Skip for now", for: .normal)
        button.backgroundColor = GlobalSettings.shared().mainColor
        button.titleLabel?.font = GlobalSettings.shared().systemFont(type: .semiBold, size: 15)
        button.setTitleColor(GlobalSettings.shared().black, for: .normal)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(didSelectSkipButton), for: .touchUpInside)
        NSLayoutConstraint.activate([
            button.widthAnchor.constraint(equalToConstant: buttonWidth),
            button.heightAnchor.constraint(equalToConstant: buttonHeight)
        ])
        loginProviderStackView.addArrangedSubview(button)
    }
    
    func addLoginImage() {
        imageView.alpha = 0
        imageView.backgroundColor = .none
        
        let image = UIImageView(image: UIImage(named: "Login Image"))
        image.translatesAutoresizingMaskIntoConstraints = false
        imageView.addSubview(image)
        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: imageView.topAnchor),
            image.leadingAnchor.constraint(equalTo: imageView.leadingAnchor),
            image.trailingAnchor.constraint(equalTo: imageView.trailingAnchor),
            image.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -20)])
        
        let label = UILabel()
        label.text = "Order it"
        label.font = GlobalSettings.shared().systemFont(type: .bold, size: 20)
        label.textColor = GlobalSettings.shared().mainColor
        label.translatesAutoresizingMaskIntoConstraints = false
        imageView.addSubview(label)
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: imageView.centerXAnchor),
            label.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 8)])
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            imageView.widthAnchor.constraint(equalToConstant: 125),
            imageView.heightAnchor.constraint(equalToConstant: 130)])
        
    }
}
