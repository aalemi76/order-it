//
//  LoginViewModel.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-25.
//

import Foundation
import GoogleSignIn
import FacebookLogin

class LoginViewModel: ViewModelProvider {
    
    let googleSignInConfig = GIDConfiguration(clientID: "611815705205-anom0h0rf55vohojb25jcd7n02gn5dbv.apps.googleusercontent.com")
    
    weak var view: Viewable?
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
    }
    
    func didTapGoogleSignIn() {
        guard let viewController = view as? LoginViewController else { return }
        
        GIDSignIn.sharedInstance.signIn(with: googleSignInConfig, presenting: viewController) { [weak self] user, error in
            
            guard error == nil, let profile = user?.profile else {
                self?.view?.show(result: .failure(.cannotRetrieveUser))
                return
            }
            
            let user = User(givenName: profile.givenName, lastName: profile.familyName, emailAddress: profile.email, profilePicURL: profile.imageURL(withDimension: 320)?.absoluteString)
            
            do {
                try UserInfoManager.shared.setUser(user)
                self?.view?.show(result: .success(true))
            } catch {
                self?.view?.show(result: .failure(.cannotSaveUser))
            }

        }
    }
    
    func didTapFacebookLogin() {
        guard let viewController = view as? LoginViewController else { return }
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile", "email"], from: viewController) { [weak self] result, error in
            
            guard error == nil, let result = result, !result.isCancelled else {
                self?.view?.show(result: .failure(.cannotRetrieveUser))
                return
            }
            
            self?.loadProfile()
        }
    }
    
    func loadProfile() {
        
        Profile.loadCurrentProfile { [weak self] profile, error in
            guard let profile = profile else {
                self?.view?.show(result: .failure(.cannotRetrieveUser))
                return
            }
            let user = User(givenName: profile.firstName, lastName: profile.lastName, emailAddress: profile.email, profilePicURL: profile.imageURL?.absoluteString)
            
            do {
                try UserInfoManager.shared.setUser(user)
                self?.view?.show(result: .success(true))
            } catch {
                self?.view?.show(result: .failure(.cannotSaveUser))
            }
        }
    }
}
