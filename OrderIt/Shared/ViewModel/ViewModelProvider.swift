//
//  ViewModelProvider.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-25.
//

import Foundation

protocol ViewModelProvider: AnyObject {
    func viewDidLoad(_ view: Viewable)
}
