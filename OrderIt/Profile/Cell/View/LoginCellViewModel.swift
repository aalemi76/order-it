//
//  LoginCellViewModel.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-28.
//

import Foundation
import Combine

class LoginCellViewModel: TableCellViewModel {
    
    let didSelectLoginButton = PassthroughSubject<Bool, Never>()
    
    func didTapLoginButton() {
        didSelectLoginButton.send(true)
    }
}
