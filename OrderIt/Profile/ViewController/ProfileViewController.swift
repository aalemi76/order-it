//
//  ProfileViewController.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-25.
//

import UIKit

class ProfileViewController: SharedViewController {
    
    let viewModel: ViewModelProvider
    
    private lazy var tableView: UITableView = {
        let tbl = UITableView(frame: .zero, style: .insetGrouped)
        tbl.backgroundColor = .none
        tbl.tableHeaderView = nil
        tbl.tableFooterView = nil
        tbl.sectionHeaderHeight = 30
        tbl.sectionFooterHeight = 0
        tbl.separatorStyle = .none
        tbl.rowHeight = UITableView.automaticDimension
        tbl.estimatedRowHeight = 300
        return tbl
    }()
    
    lazy var tableViewContainer: TableViewContainer = {
        let container = TableViewContainer(tableView: tableView)
        container.backgroundColor = GlobalSettings.shared().black
        return container
    }()
    
    init(viewModel: ViewModelProvider) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad(self)
        addTableViewContainer()
    }
    
    func addTableViewContainer() {
        tableViewContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableViewContainer)
        NSLayoutConstraint.activate([
            tableViewContainer.topAnchor.constraint(equalTo: view.topAnchor),
            tableViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableViewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
}
