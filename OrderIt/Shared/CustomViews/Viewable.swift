//
//  Viewable.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-25.
//

import Foundation

protocol Viewable: AnyObject {
    func show(result: Result<Any, OrderItError>)
}
