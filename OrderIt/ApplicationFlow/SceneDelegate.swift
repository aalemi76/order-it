//
//  SceneDelegate.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-22.
//

import UIKit
import GoogleSignIn
import FacebookCore

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        handleUserState(windowScene: windowScene)
    }
    
    func handleUserState(windowScene: UIWindowScene) {
        
        let dispatchGroup = DispatchGroup()
        
        dispatchGroup.enter()
        googleSignIn {
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        facebookLogin {
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) { [weak self] in
            self?.startApplication(windowScene: windowScene)
        }
    }
    
    func startApplication(windowScene: UIWindowScene) {
        
        let navigationController = OINavigationController()
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        appCoordinator = AppCoordinator(navigationController: navigationController)
        appCoordinator?.start()
    }
    
    func googleSignIn(onComplete: @escaping () -> Void) {
        GIDSignIn.sharedInstance.restorePreviousSignIn { user, error in
            
            if error != nil || user == nil {
                UserInfoManager.shared.isSignedIn = false
                UserInfoManager.shared.user = nil
                onComplete()
                
            } else if let profile = user?.profile {
                
                let user = User(givenName: profile.givenName, lastName: profile.familyName, emailAddress: profile.email, profilePicURL: profile.imageURL(withDimension: 320)?.absoluteString)
                
                try? UserInfoManager.shared.updateUser(user)
                onComplete()
            }
        }
    }
    
    func facebookLogin(onComplete: @escaping () -> Void) {
        Profile.loadCurrentProfile { profile, error in
            guard error == nil, let profile = profile else {
                UserInfoManager.shared.isSignedIn = false
                UserInfoManager.shared.user = nil
                onComplete()
                return
            }
            
            let user = User(givenName: profile.name, lastName: profile.lastName, emailAddress: profile.email, profilePicURL: profile.imageURL?.absoluteString)
            
            try? UserInfoManager.shared.updateUser(user)
            onComplete()
        }
    }

    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        
        guard let url = URLContexts.first?.url else {
            return
        }

        ApplicationDelegate.shared.application(
            UIApplication.shared,
            open: url,
            sourceApplication: nil,
            annotation: [UIApplication.OpenURLOptionsKey.annotation]
        )
    }

}

