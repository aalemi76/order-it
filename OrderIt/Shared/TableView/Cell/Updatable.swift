//
//  Updatable.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-26.
//

import Foundation
protocol Updatable: AnyObject {
    func attach(viewModel: Reusable)
    func update(model: Any)
}
