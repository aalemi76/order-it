//
//  LoginCoordinator.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-23.
//

import UIKit
import Combine

class LoginCoordinator: Coordinator {
    var finishDelegate: CoordinatorFinishDelegate?
    
    var type: CoordinatorType { .login }
    
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    var cancellableStore = Set<AnyCancellable>()
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.childCoordinators = []
    }
    
    func start() {
        let viewController = LoginViewController(viewModel: LoginViewModel())
        
        viewController.didFinishSignInFlow.sink { _ in
            
        } receiveValue: { [weak self] didFinish in
            if  didFinish {
                self?.finish()
            }
        }.store(in: &cancellableStore)

        push(viewController: viewController, animated: true)
    }
    
}
