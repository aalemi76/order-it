//
//  HomeCoordinator.swift
//  OrderIt
//
//  Created by AliReza on 2022-10-26.
//

import UIKit

class HomeCoordinator: Coordinator {
    var finishDelegate: CoordinatorFinishDelegate?
    
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    var type: CoordinatorType
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.childCoordinators = []
        self.type = .tab
    }
    
    func start() {
        let viewModel = HomeTabViewModel(interactor: Interactor<HomeTabModel>())
        let viewController = HomeViewController(viewModel: viewModel)
        viewController.title = "Home"
        push(viewController: viewController, animated: true)
    }
    
    
}
